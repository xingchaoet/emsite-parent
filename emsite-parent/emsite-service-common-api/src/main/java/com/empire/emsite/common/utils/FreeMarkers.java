/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * 类FreeMarkers.java的实现描述：FreeMarkers工具类
 * 
 * @author arron 2017年10月30日 下午1:14:17
 */
public class FreeMarkers {

    public static String renderString(String templateString, Map<String, ?> model) {
        try {
            StringWriter result = new StringWriter();
            Template t = new Template("name", new StringReader(templateString), new Configuration());
            t.process(model, result);
            return result.toString();
        } catch (Exception e) {
            throw Exceptions.unchecked(e);
        }
    }

    public static String renderTemplate(Template template, Object model) {
        try {
            StringWriter result = new StringWriter();
            template.process(model, result);
            return result.toString();
        } catch (Exception e) {
            throw Exceptions.unchecked(e);
        }
    }

    public static Configuration buildConfiguration(String directory) throws IOException {
        Configuration cfg = new Configuration();
        Resource path = new DefaultResourceLoader().getResource(directory);
        cfg.setDirectoryForTemplateLoading(path.getFile());
        return cfg;
    }

    public static void main(String[] args) throws IOException {
        //		// renderString
        //		Map<String, String> model = com.google.common.collect.Maps.newHashMap();
        //		model.put("userName", "calvin");
        //		String result = FreeMarkers.renderString("hello ${userName}", model);
        //		System.out.println(result);
        //		// renderTemplate
        //		Configuration cfg = FreeMarkers.buildConfiguration("classpath:/");
        //		Template template = cfg.getTemplate("testTemplate.ftl");
        //		String result2 = FreeMarkers.renderTemplate(template, model);
        //		System.out.println(result2);

        //		Map<String, String> model = com.google.common.collect.Maps.newHashMap();
        //		model.put("userName", "calvin");
        //		String result = FreeMarkers.renderString("hello ${userName} ${r'${userName}'}", model);
        //		System.out.println(result);
    }

}
